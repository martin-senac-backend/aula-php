<?php

$ListaClientes = [
    'Martin de Almeida',
    'Ana Maria',
    'Gustavo Silva'
];

$limite = 10;
$tabuada = 5;
$contagem = 0;

echo '<h1>Trabalhando com Estrutura Laço de Repetição</h1>';




echo '<h2>Exemplo de While (Enquanto..)</h2>';


while ($contagem <= 10) {
    echo $contagem++ . ' ';
}


echo "<hr>";

###########################

echo "<h2>Exemplo de do... while</h2>";

$contagem = 0;

do {
    echo $contagem++ . ' ';
} while ($contagem <= 10);

echo "<hr>";

###########################

echo "<h2>Exemplo de for (para)</h2>";

for ($contagem = 0; $contagem <= 10; $contagem++) {
    echo $contagem . ' ';
}

echo "<hr>";

###########################

echo "<h2>Exemplo de foreach</h2>";

//Ambiente de Desenvolvimento
echo var_dump($ListaClientes);

//Ambiente de Produção
echo $ListaClientes[1];

echo "<br><br>";

foreach ($ListaClientes as $key => $nome) {
    echo "O index/Chave é: $key eo valor é: $nome";
    echo "<br>";
}

echo "<br><br>";

foreach ($ListaClientes as $nome) {
    echo "Valor é: $nome";
    echo "<br>";
}
