<?php

/*
#camelCase
#snack_case
-----------------------------------------------------------
Tipos de Variaveis

-strings
-number(integer, float, double)
-boolean
-array
-object
-null



*/

$curso = 'PHP'; //string
$cargaHoraria = 120; //integer
$valorCurso = 199.90; //double
$inscricaoAberta = true; //boolean
$conteudo = ['Variável', 'Função', 'POO']; //array

class Aluno
{
};
$aluno = new Aluno(); //object

$frequencia = null; //null

echo $curso;
