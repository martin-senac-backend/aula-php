<?php

#Array Unidimensional

$listaCompra = ['Arroz', 'Feijão', 'Banana', 'Detergente', 'Sabonete'];

echo "<pre>";
print_r($listaCompra);
echo "</pre>";

echo "<hr>";

echo "<pre>";
var_dump($listaCompra);
echo "</pre>";

echo "<hr>";

echo $listaCompra[2] . " ," . $listaCompra[4];

echo "<hr>";

foreach ($listaCompra as $item) {
    echo $item . ", ";
    echo "<br>";
}

echo "<hr>";

#Array Associativo

$Funcionario = [
    "nome" =>   "Martin Almeida",
    "cargo" =>  "Programador",
    "idade" =>  17,
    "salario" =>    5000.50,
    "ativo" => true
];

var_dump($Funcionario);

echo "<hr>";

echo $Funcionario["cargo"];
