<?php
$nome = 'Martin de Almeida';
$nacionalidade = 'brasileiro';
$cargo = 'Docente';
$cpf = '222.333.444-33';
$rg = '34.111.444-X';
$cnpj = '51.747.388/0001-00';
$endereco = 'R. Paraiba, 125 - Marilia,SP,17509-060';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Teste</title>
</head>
<body>
    <h1>Declaração local de Trabalho</h1>
    <p>Eu, <?=$nome?>, <?=$nacionalidade?>, <?=$cargo?>, inscrito(a)no CPF sob o nº <?=$cpf?> e no RG
    <?=$rg?>, declaro para os devidos fins que possuo vínculo empregatício com a empresa
     inscrita no CNPJ sob o nº <?=$cnpj?>,localizada à <?=$endereco?></p>
    <br>
    <P>Por ser expressão da verdade, firmo a presente para efeitos legais.</P>
    <br>
    <p>Marília–SP,15 de Setembro de 2022</p>
    <br>
    <br>
    <br>
    <p>Martin de Almeida</p>

</body>
</html>

