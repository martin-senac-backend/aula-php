<?php

$nome = 'Martin de Almeida';
$idade = 18;
$email = 'martindealmeida17@gmail.com';
$senha = '12345678';
$cursos = ['PHP', 'HTML', 'CSS'];


echo '<h1>Trabalhando com Estrutura condicional</h1>';

echo '<h2>Exemplo de if (se...)</h2>';

if ($idade >= 18) {
    echo "O usuario $nome é maior de idade";
}

echo '<hr>';

######################################

echo "<h2>Exemplo de if e else</h2>";


if ($email == "martindealmeida17@gmail.com" && $senha == "12345678") {
    echo "Usuario Logado";
} else {
    echo "Usuario ou Senha Invalída";
}

#######################################

echo '<br>';

echo "exemplo 2: ";

if ($email == 'martindealmeida17@gmail.com') {
    if ($senha == '12345678') {
        echo "Usuario Logado";
    } else {
        echo "Usuario ou Senha Invalida";
    }
} else {
    echo "Usuario ou Senha Invalida";
}

#######################################
echo '<hr>';

echo "<h2>Exemplo de if Ternario</h2>";

echo ($idade >= 18) ? "Maior de Idade" : "Menor de Idade";



######################################
echo '<hr>';

echo "<h2>Exemplo de Multiplas Condições</h2>";

$num1 = 10;
$num2 = 20;

if ($num1 == $num2) {
    echo "Os numeros são iguais";
} else if ($num1 > $num2) {
    echo "O numero 1 e Maior que o Numero 2";
} else {
    echo "O numero 2 e Maior que o Numero 1";
}


#######################################
echo '<hr>';

echo "<h2>Exemplo de GET</h2>";

$menu = $_GET['menu'] ?? "Home";

switch (strtolower($menu)) {
    case "home":
        echo "Pagina Principal";
        break;

    case "empresa":
        echo "Pagina Empresa";
        break;


    case "produtos":
        echo "Pagina Produtos";
        break;


    case "contato":
        echo "Pagina Contato";
        break;


    default:
        echo "Pagina Erro 404";
}
