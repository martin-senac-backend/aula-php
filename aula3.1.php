<?php
$nome = 'Martin de Almeida';
$curso = 'PHP';
$frequencia = '100%';
$nota = 9;


?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Informações do Aluno</title>
</head>
<body>
    <h1>Dados do Aluno</h1>
    <p>
        <strong>Nome:</strong> <?php echo $nome; ?>
    </p>

    <p>
        <strong>Curso:</strong> <?php echo $curso; ?>
    </p>

    <p>
        <strong>frequencia:</strong> <?php echo $frequencia; ?>
    </p>

    <p>
        <strong>Nota:</strong> <?php echo $nota; ?>
    </p>

    <p>
        O aluno <?=$nome?>, Frequentou o  curso de <?=$curso?> com <?=$frequencia?> de presença
    </p>
</body>
</html>