<?php

// Variavel do Tipo String //
$nome = "Martin Almeida";

echo $nome;
    
$idade = 17;
echo "<br>";
echo $idade;

// Varivel do tipo Float //
echo "<hr>";

$salario = 100.50;

echo $salario;

echo "<hr>";

echo "R$" . number_format($salario,2,",",".");

echo "<hr>";

$nome2 ="Martin";
$sobrenome = "Almeida";

echo $nome2 ." ". $sobrenome;

echo "<hr>";

echo "A caixa d'agua esta vazia";

echo "<hr>";

//Aula de PHP//

echo "Aula de \"PHP\"";

?>